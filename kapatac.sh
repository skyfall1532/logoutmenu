#! /bin/bash

HEIGHT=15
WIDTH=40
CHOICE_HEIGHT=4
BACKTITLE="Menu"
TITLE="Kapatma & yeniden başlat"
MENU="Herhangi birini seç"

OPTIONS=(1 "Logout i3"
         2 "Logout awesome"
         3 "Shutdown"
         4 "Reboot")

CHOICE=$(dialog --clear \
                --backtitle "$BACKTITLE" \
                --title "$TITLE" \
                --menu "$MENU" \
                $HEIGHT $WIDTH $CHOICE_HEIGHT \
                "${OPTIONS[@]}" \
                2>&1 >/dev/tty)

case $CHOICE in
        1)
            i3-msg exit
            ;;
        2)
        	killall awesome
            ;;
        3)
            poweroff
            ;;
        4)
            reboot
            ;;
esac
